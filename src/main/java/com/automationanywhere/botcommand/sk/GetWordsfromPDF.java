
package com.automationanywhere.botcommand.sk;


import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.itextpdf.text.Rectangle;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Detect Words", name = "detectwordspdf",
        description = "Detect words and mark them in a document",
        node_label = "Detect Words",  icon = "pkg.svg", comment = true ,  background_color =  "#d40000" ,
        return_type=DataType.TABLE ,return_label="Table of detected words", return_required=true)

public class GetWordsfromPDF {
	   
	@Execute
    public TableValue action(@Idx(index = "1", type = AttributeType.FILE)  @Pkg(label = "PDF Input File"  , default_value_type = DataType.FILE) @NotEmpty String in,
    					      @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "PDF Output File"  , default_value_type = DataType.FILE )  @NotEmpty String out,
    					      @Idx(index = "3", type = AttributeType.LIST)  @Pkg(label = "List of words to detect"  , default_value_type = DataType.LIST , return_sub_type = STRING )  @NotEmpty List<Value> detectwords,
							  @Idx(index = "4", type = AttributeType.BOOLEAN) @Pkg(label = "Match case" , default_value_type = DataType.BOOLEAN , default_value = "false") @NotEmpty Boolean matchcase,
							  @Idx(index = "5", type = AttributeType.BOOLEAN) @Pkg(label = "Exact match" , default_value_type = DataType.BOOLEAN , default_value = "false") @NotEmpty Boolean exactmatch,
							  @Idx(index = "6", type = AttributeType.TEXT) @Pkg(label = "RGB Color for markings" , default_value_type = DataType.STRING , default_value = "51,255,255") @NotEmpty String colorcodes							  
		   ) {
					

		Table table = new Table();
		TableValue tableValue= new TableValue();
		List<String> detectwordList = new ArrayList<String>();
		for (Iterator iterator = detectwords.iterator(); iterator.hasNext();) {
			Value value = (Value) iterator.next();
			detectwordList.add(value.toString());	
		}
		int[] rgb  = Arrays.stream(colorcodes.replaceAll(" ","").split(",")).mapToInt(Integer::parseInt).toArray();
		
		try {
			List<Row> rows = new ArrayList<Row>();
			List<Schema> schemas = new ArrayList<Schema>();
			Schema schema =  new Schema();
			schema.setName("Word"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("Page"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("BoundingBox"); 
			schemas.add(schema);
			PDFUtils pdfutil = new PDFUtils();
			List<Word> result = pdfutil.markText(in, detectwordList , exactmatch, matchcase, out,rgb);
			for (Iterator iterator = result.iterator(); iterator.hasNext();) {
				Word word = (Word) iterator.next();
				List<Value> values = new ArrayList<Value>();
				values.add(new StringValue(word.getText()));
				values.add(new NumberValue(word.getPage()));
				List<Rectangle> rectangles = word.getRectangles();
				String coords = "[";
				for (Iterator iterator2 = rectangles.iterator(); iterator2.hasNext();) {
					Rectangle rectangle = (Rectangle) iterator2.next();
					coords = coords + "{\"left\":"+rectangle.getLeft()+",\"bottom\":"+rectangle.getBottom()+",\"right\":"+rectangle.getRight()+",\"top\":"+rectangle.getTop()+"}";
					if (iterator2.hasNext()) {
						coords = coords +",";
					}
				}
				coords = coords+"]";
				values.add(new StringValue(coords));
				rows.add(new Row(values));
				
			}

			table.setRows(rows);
			table.setSchema(schemas);
			tableValue.set(table);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new BotCommandException(e.getMessage())
;		}
	     

	     return tableValue;
	  }
		

}

