package com.automationanywhere.botcommand.sk;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.LineSegment;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;

public class WordRenderListener implements RenderListener
{

	List<String> breakCharacter = Arrays.asList( "." , " " , "," , ";" , "?" , "!" , "\t" , "\n" , "\u00A0" );  
	List<String> nonbreakCharacter = Arrays.asList(".","," ); 
	 private Integer page;

	 private  StringBuffer out  = new StringBuffer();
	  
	 private List<Word> words ;
	  
	 private	Word currentword;
	 
	 public WordRenderListener(List<Word> out,  Integer page) {
		 this.words = out;
		 this.page = page;

		
	 }

	  public String getResultantText() {
		    return this.out.toString();
		  }

	@Override
	public void beginTextBlock() {
		// TODO Auto-generated method stub
	
	}

	  
	protected final Word appendWord() {
		  Word word = new Word(this.page);
		  this.words.add(word);
		  return word;
	}
	  
	protected final Word mergedWord(TextRenderInfo textInfo) {
		  Word word ;
		  if (this.words.size() > 0) {
		    	word = this.words.get(this.words.size()-1);
		    	word.addText(textInfo.getText());
		        word.addGlyph(textInfo);
		    	this.words.set(this.words.size()-1, word);
		  }
		  else {
		    	word = this.words.get(0);
		    	word.addText(textInfo.getText());
		    	word.addGlyph(textInfo);
		    	this.words.set(0, word);
		  }

		    return word;
	  }
	
	@Override
	 public void renderText(TextRenderInfo renderInfo) {
	    List<TextRenderInfo> glyphs = renderInfo.getCharacterRenderInfos();
	    boolean first = true;
	    for (ListIterator<TextRenderInfo> iterator = glyphs.listIterator(); iterator.hasNext(); ) {
	      TextRenderInfo textRenderInfo = iterator.next();
	      String glyph = textRenderInfo.getText();
	      if (this.breakCharacter.contains(glyph)) {
	    	  if (iterator.hasNext()) {
	    		  TextRenderInfo nextglyphInfo = iterator.next();
	    		  if (!this.breakCharacter.contains(nextglyphInfo.getText()) && this.nonbreakCharacter.contains(glyph)) { // work break glyph seems not bie one , like comma or dot in a number 600,000
	    			  this.currentword = mergedWord(textRenderInfo);
	    			  this.currentword = mergedWord(nextglyphInfo);
	    		  }
	    		  else {
	    			  iterator.previous();
	    			  this.currentword = appendWord();
	    		  }
	    	  }
	    	  else {
	    	      this.currentword = appendWord();
	    	  }
	      } 
	      else {
	        if (this.currentword == null) {
	          this.currentword = appendWord(); 
	        }
	        this.currentword = mergedWord(textRenderInfo);
	      }
	      
	    } 
	  }


	@Override
	public void endTextBlock() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderImage(ImageRenderInfo renderInfo) {
		// TODO Auto-generated method stub
		
	}
}



