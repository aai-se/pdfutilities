package com.automationanywhere.botcommand.sk;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Glyph;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

public class Word {

	private String text = "";
	private List<TextRenderInfo> renderGlyph ;
	private Integer page;
	
   public Word(Integer page) {
	   this.page = page;
	   this.renderGlyph = new ArrayList<TextRenderInfo>();
   }
	
	public void addText(String addText) {
		this.text = this.text+addText;
	}
	
	public void addGlyph(TextRenderInfo glyph) {
		this.renderGlyph.add(glyph);
	}
	
	
	public String getText() {
		return this.text;
	}
	
	public void setText(String newtext) {
		this.text = newtext;
	}
	
	public Integer getPage() {
		return this.page;
	}
	
	public List<Rectangle> getRectangles() {
		List<Rectangle> rectangles = new ArrayList<Rectangle>();
	    Rectangle rectangle;
		for (ListIterator<TextRenderInfo> iterator = renderGlyph.listIterator(); iterator.hasNext();) {
			TextRenderInfo info = (TextRenderInfo) iterator.next();
	        rectangle = new Rectangle(new Double(info.getDescentLine().getBoundingRectange().getMinX()).floatValue(),
	    		  					  new Double(info.getDescentLine().getBoundingRectange().getMinY()).floatValue(), 
	    		  					  new Double(info.getAscentLine().getBoundingRectange().getMaxX()).floatValue(), 
		  					          new Double(info.getAscentLine().getBoundingRectange().getMaxY()).floatValue());
	        while (iterator.hasNext()) {
	        	TextRenderInfo next = (TextRenderInfo)iterator.next();
		        Rectangle nextrectangle = new Rectangle(new Double(next.getDescentLine().getBoundingRectange().getMinX()).floatValue(),
	  					  new Double(next.getDescentLine().getBoundingRectange().getMinY()).floatValue(), 
	  					  new Double(next.getAscentLine().getBoundingRectange().getMaxX()).floatValue(), 
				          new Double(next.getAscentLine().getBoundingRectange().getMaxY()).floatValue());
				if (nextrectangle.getBottom() == rectangle.getBottom()) {
					rectangle.setRight(nextrectangle.getRight());
					rectangle.setTop(nextrectangle.getTop());
				}
				else {
					iterator.previous();
					break;
				}
	      
	        }
	        rectangles.add(rectangle);
	    }
	    return rectangles;
	}

}
	
