package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetWordsfromPDFCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetWordsfromPDFCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetWordsfromPDF command = new GetWordsfromPDF();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("in") && parameters.get("in") != null && parameters.get("in").get() != null) {
      convertedParameters.put("in", parameters.get("in").get());
      if(convertedParameters.get("in") !=null && !(convertedParameters.get("in") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","in", "String", parameters.get("in").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("in") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","in"));
    }

    if(parameters.containsKey("out") && parameters.get("out") != null && parameters.get("out").get() != null) {
      convertedParameters.put("out", parameters.get("out").get());
      if(convertedParameters.get("out") !=null && !(convertedParameters.get("out") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","out", "String", parameters.get("out").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("out") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","out"));
    }

    if(parameters.containsKey("detectwords") && parameters.get("detectwords") != null && parameters.get("detectwords").get() != null) {
      convertedParameters.put("detectwords", parameters.get("detectwords").get());
      if(convertedParameters.get("detectwords") !=null && !(convertedParameters.get("detectwords") instanceof List)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","detectwords", "List", parameters.get("detectwords").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("detectwords") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","detectwords"));
    }

    if(parameters.containsKey("matchcase") && parameters.get("matchcase") != null && parameters.get("matchcase").get() != null) {
      convertedParameters.put("matchcase", parameters.get("matchcase").get());
      if(convertedParameters.get("matchcase") !=null && !(convertedParameters.get("matchcase") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","matchcase", "Boolean", parameters.get("matchcase").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("matchcase") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","matchcase"));
    }

    if(parameters.containsKey("exactmatch") && parameters.get("exactmatch") != null && parameters.get("exactmatch").get() != null) {
      convertedParameters.put("exactmatch", parameters.get("exactmatch").get());
      if(convertedParameters.get("exactmatch") !=null && !(convertedParameters.get("exactmatch") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","exactmatch", "Boolean", parameters.get("exactmatch").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("exactmatch") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","exactmatch"));
    }

    if(parameters.containsKey("colorcodes") && parameters.get("colorcodes") != null && parameters.get("colorcodes").get() != null) {
      convertedParameters.put("colorcodes", parameters.get("colorcodes").get());
      if(convertedParameters.get("colorcodes") !=null && !(convertedParameters.get("colorcodes") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","colorcodes", "String", parameters.get("colorcodes").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("colorcodes") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","colorcodes"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("in"),(String)convertedParameters.get("out"),(List<Value>)convertedParameters.get("detectwords"),(Boolean)convertedParameters.get("matchcase"),(Boolean)convertedParameters.get("exactmatch"),(String)convertedParameters.get("colorcodes")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
