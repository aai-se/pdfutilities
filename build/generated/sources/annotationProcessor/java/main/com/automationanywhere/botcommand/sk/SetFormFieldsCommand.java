package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class SetFormFieldsCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(SetFormFieldsCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    SetFormFields command = new SetFormFields();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("filein") && parameters.get("filein") != null && parameters.get("filein").get() != null) {
      convertedParameters.put("filein", parameters.get("filein").get());
      if(convertedParameters.get("filein") !=null && !(convertedParameters.get("filein") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filein", "String", parameters.get("filein").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filein") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filein"));
    }
    if(convertedParameters.containsKey("filein")) {
      String filePath= ((String)convertedParameters.get("filein"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","filein","pdf"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("pdf".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","filein","pdf"));
      }

    }
    if(parameters.containsKey("password") && parameters.get("password") != null && parameters.get("password").get() != null) {
      convertedParameters.put("password", parameters.get("password").get());
      if(convertedParameters.get("password") !=null && !(convertedParameters.get("password") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","password", "SecureString", parameters.get("password").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("fileout") && parameters.get("fileout") != null && parameters.get("fileout").get() != null) {
      convertedParameters.put("fileout", parameters.get("fileout").get());
      if(convertedParameters.get("fileout") !=null && !(convertedParameters.get("fileout") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","fileout", "String", parameters.get("fileout").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("fileout") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","fileout"));
    }
    if(convertedParameters.containsKey("fileout")) {
      String filePath= ((String)convertedParameters.get("fileout"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","fileout","pdf"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("pdf".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","fileout","pdf"));
      }

    }
    if(parameters.containsKey("fields") && parameters.get("fields") != null && parameters.get("fields").get() != null) {
      convertedParameters.put("fields", parameters.get("fields").get());
      if(convertedParameters.get("fields") !=null && !(convertedParameters.get("fields") instanceof Map)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","fields", "Map", parameters.get("fields").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("fields") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","fields"));
    }

    if(parameters.containsKey("isXFA") && parameters.get("isXFA") != null && parameters.get("isXFA").get() != null) {
      convertedParameters.put("isXFA", parameters.get("isXFA").get());
      if(convertedParameters.get("isXFA") !=null && !(convertedParameters.get("isXFA") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","isXFA", "Boolean", parameters.get("isXFA").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("flatten") && parameters.get("flatten") != null && parameters.get("flatten").get() != null) {
      convertedParameters.put("flatten", parameters.get("flatten").get());
      if(convertedParameters.get("flatten") !=null && !(convertedParameters.get("flatten") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","flatten", "Boolean", parameters.get("flatten").get().getClass().getSimpleName()));
      }
    }

    try {
      command.action((String)convertedParameters.get("filein"),(SecureString)convertedParameters.get("password"),(String)convertedParameters.get("fileout"),(Map<String, Value>)convertedParameters.get("fields"),(Boolean)convertedParameters.get("isXFA"),(Boolean)convertedParameters.get("flatten"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
