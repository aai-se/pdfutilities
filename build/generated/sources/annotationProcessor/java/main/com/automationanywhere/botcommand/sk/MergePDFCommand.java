package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class MergePDFCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(MergePDFCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    MergePDF command = new MergePDF();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("file1") && parameters.get("file1") != null && parameters.get("file1").get() != null) {
      convertedParameters.put("file1", parameters.get("file1").get());
      if(convertedParameters.get("file1") !=null && !(convertedParameters.get("file1") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","file1", "String", parameters.get("file1").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("file1") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","file1"));
    }
    if(convertedParameters.containsKey("file1")) {
      String filePath= ((String)convertedParameters.get("file1"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","file1","pdf"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("pdf".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","file1","pdf"));
      }

    }
    if(parameters.containsKey("password1") && parameters.get("password1") != null && parameters.get("password1").get() != null) {
      convertedParameters.put("password1", parameters.get("password1").get());
      if(convertedParameters.get("password1") !=null && !(convertedParameters.get("password1") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","password1", "SecureString", parameters.get("password1").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("file2") && parameters.get("file2") != null && parameters.get("file2").get() != null) {
      convertedParameters.put("file2", parameters.get("file2").get());
      if(convertedParameters.get("file2") !=null && !(convertedParameters.get("file2") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","file2", "String", parameters.get("file2").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("file2") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","file2"));
    }
    if(convertedParameters.containsKey("file2")) {
      String filePath= ((String)convertedParameters.get("file2"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","file2","pdf"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("pdf".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","file2","pdf"));
      }

    }
    if(parameters.containsKey("password2") && parameters.get("password2") != null && parameters.get("password2").get() != null) {
      convertedParameters.put("password2", parameters.get("password2").get());
      if(convertedParameters.get("password2") !=null && !(convertedParameters.get("password2") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","password2", "SecureString", parameters.get("password2").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("outputfile") && parameters.get("outputfile") != null && parameters.get("outputfile").get() != null) {
      convertedParameters.put("outputfile", parameters.get("outputfile").get());
      if(convertedParameters.get("outputfile") !=null && !(convertedParameters.get("outputfile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","outputfile", "String", parameters.get("outputfile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("outputfile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","outputfile"));
    }

    try {
      command.action((String)convertedParameters.get("file1"),(SecureString)convertedParameters.get("password1"),(String)convertedParameters.get("file2"),(SecureString)convertedParameters.get("password2"),(String)convertedParameters.get("outputfile"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
