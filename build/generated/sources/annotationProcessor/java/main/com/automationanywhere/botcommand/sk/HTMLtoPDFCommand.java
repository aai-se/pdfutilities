package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class HTMLtoPDFCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(HTMLtoPDFCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    HTMLtoPDF command = new HTMLtoPDF();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("htmlfile") && parameters.get("htmlfile") != null && parameters.get("htmlfile").get() != null) {
      convertedParameters.put("htmlfile", parameters.get("htmlfile").get());
      if(convertedParameters.get("htmlfile") !=null && !(convertedParameters.get("htmlfile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","htmlfile", "String", parameters.get("htmlfile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("htmlfile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","htmlfile"));
    }

    if(parameters.containsKey("pdffile") && parameters.get("pdffile") != null && parameters.get("pdffile").get() != null) {
      convertedParameters.put("pdffile", parameters.get("pdffile").get());
      if(convertedParameters.get("pdffile") !=null && !(convertedParameters.get("pdffile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","pdffile", "String", parameters.get("pdffile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("pdffile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","pdffile"));
    }

    if(parameters.containsKey("pagesize") && parameters.get("pagesize") != null && parameters.get("pagesize").get() != null) {
      convertedParameters.put("pagesize", parameters.get("pagesize").get());
      if(convertedParameters.get("pagesize") !=null && !(convertedParameters.get("pagesize") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","pagesize", "String", parameters.get("pagesize").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("pagesize") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","pagesize"));
    }
    if(convertedParameters.get("pagesize") != null) {
      switch((String)convertedParameters.get("pagesize")) {
        case "A3" : {

        } break;
        case "A4" : {

        } break;
        case "A5" : {

        } break;
        case "Tabloid" : {

        } break;
        case "Legal" : {

        } break;
        case "Letter" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","pagesize"));
      }
    }

    if(parameters.containsKey("orientation") && parameters.get("orientation") != null && parameters.get("orientation").get() != null) {
      convertedParameters.put("orientation", parameters.get("orientation").get());
      if(convertedParameters.get("orientation") !=null && !(convertedParameters.get("orientation") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","orientation", "String", parameters.get("orientation").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("orientation") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","orientation"));
    }
    if(convertedParameters.get("orientation") != null) {
      switch((String)convertedParameters.get("orientation")) {
        case "Portrait" : {

        } break;
        case "Landscape" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","orientation"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("htmlfile"),(String)convertedParameters.get("pdffile"),(String)convertedParameters.get("pagesize"),(String)convertedParameters.get("orientation")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
