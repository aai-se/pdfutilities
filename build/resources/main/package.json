{
    "name": "PDFUtils",
    "label": "PDFUtils",
    "description": "Provides actions for PDF operations.",
    "group": "com.automationanywhere",
    "artifactName": "PDFUtils",
    "packageVersion": "2.0.7-20211212-213000",
    "codeVersion": "2.0.7-20211212-213000",
    "commands": [
        {
            "name": "gettext",
            "label": "Get Full Text",
            "description": "Get Full Text",
            "nodeLabel": "Get Full Text",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.GetFullTextCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "returnType": "STRING",
            "returnSubtype": "UNDEFINED",
            "returnLabel": "Text",
            "returnRequired": true,
            "attributes": [
                {
                    "name": "file",
                    "label": "Adobe File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "password",
                    "label": "Password",
                    "type": "CREDENTIAL"
                }
            ]
        },
        {
            "name": "detectwordspdf",
            "label": "Detect Words",
            "description": "Detect words and mark them in a document",
            "nodeLabel": "Detect Words",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.GetWordsfromPDFCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "returnType": "TABLE",
            "returnSubtype": "UNDEFINED",
            "returnLabel": "Table of detected words",
            "returnRequired": true,
            "attributes": [
                {
                    "name": "in",
                    "label": "PDF Input File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "out",
                    "label": "PDF Output File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "detectwords",
                    "label": "List of words to detect",
                    "returnSubtype": "STRING",
                    "type": "LIST",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "matchcase",
                    "label": "Match case",
                    "type": "BOOLEAN",
                    "defaultValue": {
                        "boolean": false,
                        "type": "BOOLEAN"
                    },
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "exactmatch",
                    "label": "Exact match",
                    "type": "BOOLEAN",
                    "defaultValue": {
                        "boolean": false,
                        "type": "BOOLEAN"
                    },
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "colorcodes",
                    "label": "RGB Color for markings",
                    "type": "TEXT",
                    "defaultValue": {
                        "string": "51,255,255",
                        "type": "STRING"
                    },
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                }
            ]
        },
        {
            "name": "htmltopdf",
            "label": "HTML to PDF",
            "description": "Converts a HTML file to a PDF file",
            "nodeLabel": "HTML to PDF",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.HTMLtoPDFCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "returnType": "STRING",
            "returnSubtype": "UNDEFINED",
            "returnLabel": "Status",
            "returnRequired": false,
            "attributes": [
                {
                    "name": "htmlfile",
                    "label": "HTML Input File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "pdffile",
                    "label": "PDF Output File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "pagesize",
                    "label": "Orientation",
                    "type": "SELECT",
                    "defaultValue": {
                        "string": "A4",
                        "type": "STRING"
                    },
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ],
                    "options": [
                        {
                            "label": "A3",
                            "value": "A3"
                        },
                        {
                            "label": "A4",
                            "value": "A4"
                        },
                        {
                            "label": "A5",
                            "value": "A5"
                        },
                        {
                            "label": "Tabloid",
                            "value": "Tabloid"
                        },
                        {
                            "label": "Legal",
                            "value": "Legal"
                        },
                        {
                            "label": "Letter",
                            "value": "Letter"
                        }
                    ]
                },
                {
                    "name": "orientation",
                    "label": "Page Size",
                    "type": "SELECT",
                    "defaultValue": {
                        "string": "portrait",
                        "type": "STRING"
                    },
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ],
                    "options": [
                        {
                            "label": "Portrait",
                            "value": "Portrait"
                        },
                        {
                            "label": "Landscape",
                            "value": "Landscape"
                        }
                    ]
                }
            ]
        },
        {
            "name": "mergedirpdf",
            "label": "Merge all PDF files",
            "description": "Merge all PDF files in a directory into one",
            "nodeLabel": "Merge all PDF files",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.MergeDirPDFCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "attributes": [
                {
                    "name": "dir",
                    "label": "Directory with PDF files",
                    "type": "TEXT",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "outputfile",
                    "label": "Resulting File",
                    "type": "TEXT",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                }
            ]
        },
        {
            "name": "mergepdf",
            "label": "Merge PDF files",
            "description": "Merge two PDF files into one",
            "nodeLabel": "Merge PDF",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.MergePDFCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "attributes": [
                {
                    "name": "file1",
                    "label": "1. Source File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "password1",
                    "label": "Password for 1.",
                    "type": "CREDENTIAL"
                },
                {
                    "name": "file2",
                    "label": "2. Source File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "password2",
                    "label": "Password for 2.",
                    "type": "CREDENTIAL"
                },
                {
                    "name": "outputfile",
                    "label": "Resulting File",
                    "type": "TEXT",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                }
            ]
        },
        {
            "name": "readform",
            "label": "Read Form Fields",
            "description": "Read Form Fields",
            "nodeLabel": "Read Form Fields",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.ReadFormFieldsCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "returnType": "DICTIONARY",
            "returnSubtype": "STRING",
            "returnLabel": "Form Fields",
            "returnRequired": true,
            "attributes": [
                {
                    "name": "file",
                    "label": "Adobe Form File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "password",
                    "label": "Password",
                    "type": "CREDENTIAL"
                }
            ]
        },
        {
            "name": "setform",
            "label": "Set Form Fields",
            "description": "Set Form Fields",
            "nodeLabel": "Set Form Fields",
            "minimumControlRoomVersion": "8750",
            "minimumBotAgentVersion": "20.11",
            "mainClass": "com.automationanywhere.botcommand.sk.SetFormFieldsCommand",
            "icon": "pkg.svg",
            "backgroundColor": "#d40000",
            "comment": true,
            "attributes": [
                {
                    "name": "filein",
                    "label": "Input Adobe Form File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "password",
                    "label": "Password",
                    "type": "CREDENTIAL"
                },
                {
                    "name": "fileout",
                    "label": "Output Adobe Form File",
                    "type": "FILE",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        },
                        {
                            "name": "FILE_EXTENSION",
                            "value": ".pdf"
                        }
                    ]
                },
                {
                    "name": "fields",
                    "label": "Form Fields",
                    "type": "DICTIONARY",
                    "rules": [
                        {
                            "name": "NOT_EMPTY"
                        }
                    ]
                },
                {
                    "name": "isXFA",
                    "label": "XFA Form",
                    "type": "BOOLEAN",
                    "defaultValue": {
                        "boolean": false,
                        "type": "BOOLEAN"
                    }
                },
                {
                    "name": "flatten",
                    "label": "Save non-editable",
                    "type": "BOOLEAN",
                    "defaultValue": {
                        "boolean": false,
                        "type": "BOOLEAN"
                    }
                }
            ]
        }
    ],
    "author": ""
}